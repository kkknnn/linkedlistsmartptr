#include <iostream>

#include "smartpointerlist.h"



using namespace std;

int main()
{
    cout << "Hello World!" << endl;
    List<int> a;
    a.push_back(30);
    a.push_back(40);
    a.push_back(40);
    a.push_back(20);
    a.pop_front();
    a.print();
    return 0;
}
//lista dwukierunka z smartpointerami
// metoda przechodzenia do tyłu
