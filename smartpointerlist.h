#ifndef LISTTEMP_H
#define LISTTEMP_H

#include <iostream>
#include <memory>


template <class T>
class List
{
    public:
    List()=default;
    void push_back(T i);
    T pop_front();
    void print();
    ~List();

private:
    struct ListNode
    {
            T _value;
            std::shared_ptr<ListNode>_next;
    };

    std::shared_ptr<ListNode> _head;
    std::shared_ptr<ListNode> _tail;

    int _elem_num = 0;
};


#include "smartpointerlist.tpp"

#endif // LISTTEMP_H





