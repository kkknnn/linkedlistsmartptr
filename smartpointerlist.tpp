#include "smartpointerlist.h"
#include <iostream>
using namespace std;


// may be ready
template<class T>
void List<T>::push_back(T i)
{
    std::shared_ptr<ListNode> Node = std::make_shared<ListNode>();
   Node->_value = i;
   _elem_num++;
    if (_tail!=nullptr)
    {
        _tail->_next=Node;
    }

    _tail=Node;
    if (nullptr==_head)
    {

        _head=Node;
    }

    return;

}

template<class T>
List<T>::~List() {

}


template<class T>
T List<T>::pop_front()
{


    if (_elem_num>0)
    {
        _elem_num--;
        T element_to_return = _head->_value;
        auto Temp = _head->_next;

        _head = Temp;

        if (_elem_num==0)

        {   _head=nullptr;
            _tail=nullptr;
        }
        return element_to_return;
    }

    else
    {
       std::cout <<"brak elementow w kolejce"<< "\n";
    }

}

template<class T>
void List<T>::print()
{
    if (_elem_num!=0)
    {


        auto iterator = _head;
        int i = 1;

        while(true)
        {

            std::cout<< "element #" << i<< " : "<< iterator->_value << '\n';
            auto temp=iterator->_next;
            i++;

            if (iterator == _tail)
            {
                break;
            }
            iterator = temp;

        }
    }
    else
    {
        std::cout <<"brak elementow w kolejce"<< "\n";
    }
}
